package com.example.demo.config;

import com.example.demo.services.AnotherService;
import com.example.demo.services.SomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Configuration
public class Autowire {

    @Autowired
    GenericApplicationContext ctx;


    SomeService someService(int num) {

        SomeService obj = new SomeService(num);
        ctx.getAutowireCapableBeanFactory().autowireBeanProperties(obj, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false);

        return obj;
    }


    @Bean
    List<SomeService> services() {

        return IntStream.range(0, 10)
                .mapToObj(this::someService)
                .collect(Collectors.toList());
    }

    @Bean
    AnotherService anotherService() {
        return new AnotherService();
    }

}
