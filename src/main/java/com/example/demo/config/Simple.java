package com.example.demo.config;

import com.example.demo.services.AnotherService;
import com.example.demo.services.SomeService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Configuration
public class Simple {

    @Bean
    List<SomeService> services() {

        return IntStream.range(0, 10)
                .mapToObj(SomeService::new)
                .collect(Collectors.toList());
    }


    @Bean
    AnotherService anotherService() {
        return new AnotherService();
    }
}
