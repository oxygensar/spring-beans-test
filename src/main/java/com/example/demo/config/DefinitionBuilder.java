package com.example.demo.config;

import com.example.demo.services.AnotherService;
import com.example.demo.services.SomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Configuration
public class DefinitionBuilder {

    @Autowired
    GenericApplicationContext ctx;


    SomeService someService(int num) {

        BeanDefinitionBuilder bdb = BeanDefinitionBuilder.genericBeanDefinition(SomeService.class);
        bdb.setScope(ConfigurableBeanFactory.SCOPE_PROTOTYPE);
        bdb.addConstructorArgValue(num);
        AbstractBeanDefinition def = bdb.getBeanDefinition();

        String beanName = "someService" + num;
        ctx.registerBeanDefinition(beanName, def);

        return ctx.getBean(beanName, SomeService.class);
    }


    @Bean
    List<SomeService> services() {

        return IntStream.range(0, 10)
                .mapToObj(this::someService)
                .collect(Collectors.toList());
    }

    @Bean
    AnotherService anotherService() {
        return new AnotherService();
    }

}
