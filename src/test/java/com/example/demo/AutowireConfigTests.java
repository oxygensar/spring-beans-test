package com.example.demo;

import com.example.demo.config.Autowire;
import com.example.demo.services.SomeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Autowire.class)
public class AutowireConfigTests {

    @Autowired
    List<SomeService> services;

    @Test
    public void servicesWithPrototypeAutowired() {

        Assert.assertEquals(10, services.size());

        Assert.assertNotNull(services.get(0).anotherService);

    }
}
