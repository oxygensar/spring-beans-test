package com.example.demo.services;

import org.springframework.beans.factory.annotation.Autowired;

public class SomeService {

    @Autowired
    public AnotherService anotherService;

    public final int num;

    public SomeService(int num) {
        this.num = num;
    }
}
