package com.example.demo;

import com.example.demo.config.Simple;
import com.example.demo.services.SomeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Simple.class)
public class SimpleConfigTests {

    @Autowired
    List<SomeService> services;

    @Test
    public void servicesCreated() {

        Assert.assertEquals(10, services.size());

        Assert.assertNull(services.get(0).anotherService);

    }
}
